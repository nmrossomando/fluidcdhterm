#!/usr/bin/env python
#
# Terminal to talk to do "proper" C&DH via RS-232.
# Supports the fluidSW command and log message JSON formats.
# Uses COBS encoding.
# Optionally uses CCSDS packets.
#

import os
import serial
#from tkinter import *
import tkinter as tk
from tkinter import ttk
import binascii
import copy

from .FluidCDHUtils import FluidCmdEncoder, FluidLogDecoder

class FluidCmdTerm(tk.Frame):
	def __init__(self, master=None):
		super().__init__(master)
		self.pack()
		self.serialInstance = serial.Serial()
		self.cmd_encoder = FluidCmdEncoder()
		self.log_decoder = FluidLogDecoder()
		self.msg_packets = []
		self.msg_packets.append(bytes())
		self.build_gui()

	def build_gui(self):
		self.serialPanel = tk.Frame(self)
		self.serialPanel.pack(expand=1)
		
		self.serialOpen = tk.Button(self.serialPanel)
		self.serialOpen["text"] = "Open Port"
		self.serialOpen["command"] = self.serial_open
		self.serialOpen.pack(side="left")

		self.portLabel = tk.Label(self.serialPanel)
		self.portLabel["text"] = "Serial Port:"
		self.portLabel.pack(side="left")

		self.portField = tk.Entry(self.serialPanel)
		self.portField["width"] = 20
		self.portField.delete(0, tk.END)
		self.portField.insert(0, "/dev/ttyUSB0")
		self.portField.pack(side="left")

		self.baudLabel = tk.Label(self.serialPanel)
		self.baudLabel["text"] = "Baud Rate:"
		self.baudLabel.pack(side="left")

		self.baudField = tk.Entry(self.serialPanel)
		self.baudField["width"] = 8
		self.baudField.delete(0, tk.END)
		self.baudField.insert(0, "19200")
		self.baudField.pack(side="left")

		self.dataBitsLabel = tk.Label(self.serialPanel)
		self.dataBitsLabel["text"] = "Data Bits:"
		self.dataBitsLabel.pack(side="left")

		self.dataBitsField = tk.Entry(self.serialPanel)
		self.dataBitsField["width"] = 3
		self.dataBitsField.delete(0, tk.END)
		self.dataBitsField.insert(0, "8")
		self.dataBitsField.pack(side="left")

		self.parityLabel = tk.Label(self.serialPanel)
		self.parityLabel["text"] = "Parity:"
		self.parityLabel.pack(side="left")

		self.parityField = tk.Entry(self.serialPanel)
		self.parityField["width"] = 3
		self.parityField.delete(0, tk.END)
		self.parityField.insert(0, "N")
		self.parityField.pack(side="left")

		self.stopBitsLabel = tk.Label(self.serialPanel)
		self.stopBitsLabel["text"] = "Stop Bits:"
		self.stopBitsLabel.pack(side="left")

		self.stopBitsField = tk.Entry(self.serialPanel)
		self.stopBitsField["width"] = 3
		self.stopBitsField.delete(0, tk.END)
		self.stopBitsField.insert(0, "1")
		self.stopBitsField.pack(side="left")

		self.serialClose = tk.Button(self.serialPanel)
		self.serialClose["text"] = "Close Port"
		self.serialClose["command"] = self.serial_close
		self.serialClose.pack(side="left")

		self.statusBar = tk.Label(self)
		self.statusBar["text"] = " "
		self.statusBar.pack(side="bottom")

		self.commandPanel = tk.Frame(self)
		self.commandPanel.pack(side="bottom")

		self.commandBuildLabel = tk.Label(self.commandPanel)
		self.commandBuildLabel["text"] = "Command Selector:"
		self.commandBuildLabel.pack(side="left")

		self.commandBuildPicker = ttk.Combobox(self.commandPanel, values=[], width=40, state="readonly")
		self.commandBuildPicker.bind("<<ComboboxSelected>>", self.build_cmd_proto)
		self.commandBuildPicker.pack(side="left")

		"""
		self.lineEndLabel = tk.Label(self.commandPanel)
		self.lineEndLabel["text"] = "Line Ending:"
		self.lineEndLabel.pack(side="left")

		self.lineEnd = ttk.Combobox(self.commandPanel, values=["\\n", "\\0", "None"], width=5, state="readonly")
		self.lineEnd.set("\\n")
		self.lineEnd.bind("<<ComboboxSelected>>",self.clear_wrap)
		self.lineEnd.pack(side="left")
		"""

		self.commandBar = tk.Entry(self)
		self.commandBar["font"] = "monospace 10"
		self.commandBar["width"] = 80
		self.commandBar.pack(side="bottom")
		self.commandBar.bind("<Key-Return>", self.serial_send)
		
		self.outputTerm = tk.Text(self)
		self.outputTerm["font"] = "monospace 10"
		self.outputTerm["width"] = 80
		self.outputTerm["height"] = 24
		self.outputTerm["state"] = "disabled"
		self.outputTerm["wrap"] = "none"
		self.outputTerm.pack(side="bottom")
		self.serial_get()

		self.dictPanel = tk.Frame(self)
		self.dictPanel.pack(side="bottom")

		self.cmdDictLabel = tk.Label(self.dictPanel)
		self.cmdDictLabel["text"] = "Cmd Dictionary Path:"
		self.cmdDictLabel.pack(side="left")

		self.cmdDictField = tk.Entry(self.dictPanel)
		self.cmdDictField["width"] = 40
		self.cmdDictField.delete(0, tk.END)
		self.cmdDictField.insert(0, "cmd_dict.json??")
		self.cmdDictField.pack(side="left")

		self.logDictLabel = tk.Label(self.dictPanel)
		self.logDictLabel["text"] = "Log Dictionary Path:"
		self.logDictLabel.pack(side="left")

		self.logDictField = tk.Entry(self.dictPanel)
		self.logDictField["width"] = 40
		self.logDictField.delete(0, tk.END)
		self.logDictField.insert(0, "log_dict.json??")
		self.logDictField.pack(side="left")

		self.dictOpen = tk.Button(self.dictPanel)
		self.dictOpen["text"] = "Load Dictionaries"
		self.dictOpen["command"] = self.load_dicts
		self.dictOpen.pack(side="left")

	def serial_open(self):
		self.set_stat_bar("Opening Serial Port...")

		port = self.portField.get()
		baud = int(self.baudField.get())
		dataBits = int(self.dataBitsField.get())
		parity = self.parityField.get()
		stopBits = int(self.stopBitsField.get())

		if not os.path.exists(port):
			self.set_stat_bar("ERROR opening port: " + port + " not found!", "red")
			return
		elif baud not in serial.Serial.BAUDRATES:
			self.set_stat_bar("ERROR opening port: " + str(baud) + " not supported baud rate!", "red")
			return
		elif dataBits not in serial.Serial.BYTESIZES:
			self.set_stat_bar("ERROR opening port: " + str(dataBits) + " bits not supported data width!", "red")
			return
		elif parity not in ('N','E','O'):
			self.set_stat_bar("ERROR opening port: " + parity + " not supported parity!", "red")
			return
		elif stopBits not in serial.Serial.STOPBITS:
			self.set_stat_bar("ERROR opening port: " + str(stopBits) + " stop bits not supported!", "red")
			return

		self.serialInstance = serial.Serial(port=port, baudrate=baud, parity=parity, stopbits=stopBits, bytesize=dataBits)
		if not self.serialInstance.is_open:
			self.set_stat_bar("ERROR opening serial port. Unknown issue :/", "red")
			return
		else:
			self.set_stat_bar("Serial port at " + port + " open, with settings: " + str(baud) + ", " + str(dataBits) + parity + str(stopBits))
			return

	def serial_close(self,teardown=False):
		if not teardown:
			self.set_stat_bar("Closing Serial Port")
		if self.serialInstance.is_open:
			self.serialInstance.close()

	def serial_send(self, event, command=""):
		if command == "":
			command = self.commandBar.get()
			self.commandBar.delete(0, tk.END)

		if command == '':
			return
		elif command[0] == '~':
			self.terminal_command(command)
		else:
			if self.serialInstance.is_open:
				sent = 0
				compiled_cmd = self.cmd_encoder.compile_cmd(command)
				sent += self.serialInstance.write(bytearray(compiled_cmd))
				self.set_stat_bar("Sent " + str(sent) + " bytes.")
			else:
				self.set_stat_bar("ERROR: Serial port not open.","red")

	def serial_get(self):
		self.outputTerm["state"] = "normal"
		if self.serialInstance.is_open:
			# TODO Replace with table view.
			while self.serialInstance.inWaiting() > 0:
				data = self.serialInstance.read()
				self.msg_packets[-1] += data
				if data[0] == 0:
					self.msg_packets.append(bytes())
			packets_copy = copy.deepcopy(self.msg_packets)
			for msg_packet in packets_copy:
				if (len(msg_packet) > 0) and (msg_packet[-1] == 0):
					try:
						level, name, msg = self.log_decoder.decode_log_message(msg_packet)
						self.msg_packets.remove(msg_packet)
						self.outputTerm.insert("end", level + ", ")
						self.outputTerm.insert("end", name + ", ")
						self.outputTerm.insert("end", msg + "\n")
						self.outputTerm.see("end")
					except Exception as e:
						self.set_stat_bar("ERROR: Packet decode failure.","red")
						print(e)
						self.msg_packets.remove(msg_packet)

		self.outputTerm["state"] = "disabled"
		self.outputTerm.after(10, self.serial_get)

	def load_dicts(self):
		cmd_path = self.cmdDictField.get()
		log_path = self.logDictField.get()

		if cmd_path == "":
			self.set_stat_bar("Could not open dictionaries! Cmd Dict Path Empty!", color="red")
			return
		elif log_path == "":
			self.set_stat_bar("Could not open dictionaries! Log Dict Path Empty!", color="red")
			return

		self.cmd_encoder.load_cmd_dictionary(cmd_path)
		self.log_decoder.load_log_dictionary(log_path)

		cmds = self.cmd_encoder.list_commands()
		self.commandBuildPicker["values"] = cmds
		self.commandBuildPicker.set(cmds[0])

		if self.cmd_encoder.get_app_name() != self.log_decoder.get_app_name():
			self.set_stat_bar("Warning! CMD and LOG Dictionaries appear to be for different apps! Weirdness may ensue!", color="red")

	def build_cmd_proto(self, event):
		cmd_selected = self.commandBuildPicker.get()
		cmd_str = self.cmd_encoder.create_command_prototype(cmd_selected)
		self.commandBar.delete(0, tk.END)
		self.commandBar.insert(0, cmd_str)

	def set_stat_bar(self, string, color="black"):
		self.statusBar["fg"] = color
		self.statusBar["text"] = string

	def terminal_command(self, command):
		if command == "~clear":
			self.outputTerm["state"] = "normal"
			self.outputTerm.delete("1.0", tk.END)
			self.outputTerm["state"] = "disabled"
			return
		elif command == "~copy":
			self.outputTerm["state"] = "normal"
			self.clipboard_clear()
			self.clipboard_append(self.outputTerm.get("sel.first","sel.last"))
			self.outputTerm["state"] = "disabled"
			return
		elif "~dump" in command:
			dmpcmd = command.split(" ")
			if (len(dmpcmd) != 2) or (dmpcmd[1] == ""):
				self.set_stat_bar("ERROR: ~dump takes one argument, a filename. Try again! (~dump myfile.txt)", "red")
			else:
				# TODO: Make work with EVRs
				with open(dmpcmd[1],'w') as f:
					f.write(self.outputTerm.get("1.0","end"))
				self.set_stat_bar("Dumped terminal contents to file '" + dmpcmd[1] + "'")
			return
		elif "~run" in command:
			runcmd = command.split(" ")
			if (len(runcmd) != 2) or (runcmd[1] == ""):
				self.set_stat_bar("ERROR: ~run takes one argument, a filename. Try again! (~run script.fos)", "red")
			else:
				self.set_stat_bar("Running commands found in file '" + runcmd[1] + "'")
				callList = []
				with open(runcmd[1], "r") as f:
					for line in f:
						if (line.rstrip('\n') != "") and ("#" not in line):
							callList.append(line.rstrip('\n'))
				self.run_script(callList)
			return
		elif command == "~close":
			self.serial_close()
		elif command == "~open":
			self.serial_open()
		elif "~sleep" in command:
			self.set_stat_bar("ERROR: ~sleep is only meant to be used in scripts.", "red")
			return
		else:
			self.set_stat_bar("ERROR: Terminal command failed (not found/wrong args?)", "red")
			return

	def run_script(self, cmds):
		cmd = cmds[0]
		remaining = cmds[1:]

		if "~sleep" in cmd:
			sleep = cmd.split(" ")
			if (len(sleep) != 2) or (sleep[1] == ""):
				self.set_stat_bar("ERROR: Script aborted! ~sleep takes one argument, a time in milliseconds.", "red")
				return
			else:
				if len(remaining) == 0:
					self.set_stat_bar("Script run complete.")
					return
				else:
					self.after(int(sleep[1]),self.run_script,remaining)
		else:
			self.serial_send(False, cmd)
			if len(remaining) == 0:
				self.set_stat_bar("Script run complete.")
				return
			else:
				self.run_script(remaining)

if __name__ == "__main__":
	# Set up GUI
	root = tk.Tk()
	app = FluidCmdTerm(master=root)
	app.master.title("FluidSW C&DH Console")

	# Run app
	app.mainloop()

	# Teardown safety stuff
	app.serial_close(True)
