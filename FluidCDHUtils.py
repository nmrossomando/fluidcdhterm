# Utilities for command and log message processing.
# Supports loading of cmd & log message dictionaries.
# Also supports command compilation, CCSDS packetizing, and encoding with COBS.
# Also also supports log message decoding from COBS, CCSDS depacketization, and message decoding.

# TODO: Support CCSDS space packets...

from cobs import cobs
import json

def encode_for_xmit(data):
    data_out = bytearray(cobs.encode(data))
    data_out.append(0)
    return bytes(data_out)

def decode_from_recv(data):
    return cobs.decode(data.rstrip(b'\x00'))

class FluidCmdCompileError(Exception):
    def __init__(self, message="An exception occured during command compilation!"):
        self.message = message
        super().__init__(self.message)

class FluidCmdEncoder():
    def __init__(self):
        self.cmd_dict = {}
        self.special_types = {}
        self.app_name = ""
        self.opcode_length = 0
        self.use_ccsds = False

    def is_initialized(self):
        if not self.cmd_dict:
            return False
        else:
            return True

    def load_cmd_dictionary(self, cmd_dict_path):
        with open(cmd_dict_path, "r") as f:
            full_command_dict = json.load(f)

        # Add later: file validation...
        self.app_name = full_command_dict["app_name"]
        self.opcode_length = full_command_dict["opcode_len"]
        self.use_ccsds = full_command_dict["use_ccsds"]
        self.cmd_dict = {cmd["name"]: cmd for cmd in full_command_dict["commands"]}
        self.special_types = full_command_dict["types"]

    def list_commands(self):
        return [x for x in self.cmd_dict]

    def get_types(self):
        return self.special_types

    def get_app_name(self):
        return self.app_name

    def create_command_prototype(self, cmd_name):
        cmd_data = self.cmd_dict[cmd_name]
        out_str = cmd_name

        if cmd_data["args"]:
            for arg in cmd_data["args"]:
                out_str += " "
                out_str += arg["name"]

        return out_str

    def compile_cmd(self, cmd_str, encode=True):
        cmd_elements = cmd_str.split(" ")
        compiled_cmd = bytearray()

        # Get command name and verify it exists:
        cmd_name = cmd_elements[0]
        if(cmd_name not in self.cmd_dict):
            raise FluidCmdCompileError(message="Command {} not found in command dictionary!".format(cmd_name))
        cmd_compile_info = self.cmd_dict[cmd_name]

        # Get command length (number of elements) and verify it is correct:
        cmd_length = 1 + len(cmd_compile_info["args"])
        if(len(cmd_elements) != cmd_length) and ("ARRAY" not in [arg["type"] for arg in cmd_compile_info["args"]]):
            raise FluidCmdCompileError(message="Command {} was expecting {} arguments, but {} were found!".format(cmd_name, cmd_length-1, len(cmd_elements)-1))

        # "Compile" the opcode first:
        compiled_cmd.extend(cmd_compile_info["opcode"].to_bytes(self.opcode_length, 'big'))

        # Then, do args...if there are args:
        if cmd_compile_info["args"]:
            compiled_cmd.extend(self._compile_cmd_args(cmd_elements[1:], cmd_compile_info))

        if encode == True:
            #TODO: Add CCSDS
            compiled_cmd = encode_for_xmit(compiled_cmd)

        return compiled_cmd

    def _compile_cmd_args(self, input_args, cmd_info):
        compiled_args = bytearray()

        # This makes life easier if args reference other args (like array referencing a length arg.)
        args_dict = {arg["name"]: arg for arg in cmd_info["args"]}

        idx_array_offset = 0 # Account for arrays taking more than one "argument" slot in a command.
        for arg in args_dict:
            val = input_args[args_dict[arg]["idx"] + idx_array_offset]
            match args_dict[arg]["type"]:
                case "U8":
                    if (int(val) < args_dict[arg]["min"]) or (int(val) > args_dict[arg]["max"]):
                        raise FluidCmdCompileError(message="Argument {} of command {} has value {} outside allowed range {}-{}!".format(arg, cmd_info["name"], val, args_dict[arg]["min"], args_dict[arg]["max"]))
                    args_dict[arg]["value"] = int(val)
                    compiled_args.extend(int(val).to_bytes(1, 'big'))
                case "U16":
                    if (int(val) < args_dict[arg]["min"]) or (int(val) > args_dict[arg]["max"]):
                        raise FluidCmdCompileError(message="Argument {} of command {} has value {} outside allowed range {}-{}!".format(arg, cmd_info["name"], val, args_dict[arg]["min"], args_dict[arg]["max"]))
                    args_dict[arg]["value"] = int(val)
                    compiled_args.extend(int(val).to_bytes(2, 'big'))
                case "U32":
                    if (int(val) < args_dict[arg]["min"]) or (int(val) > args_dict[arg]["max"]):
                        raise FluidCmdCompileError(message="Argument {} of command {} has value {} outside allowed range {}-{}!".format(arg, cmd_info["name"], val, args_dict[arg]["min"], args_dict[arg]["max"]))
                    args_dict[arg]["value"] = int(val)
                    compiled_args.extend(int(val).to_bytes(4, 'big'))
                case "LOGIC":
                    # "LOGIC" means a binary state; allows ON/OFF, HIGH/LOW, or TRUE/FALSE as values.
                    # This can lead to odd things like POWER FALSE as a command, but that still sorta makes sense.
                    # Might still want to sharpen that pencil later.
                    if val.upper() not in ['HIGH', 'LOW', 'TRUE', 'FALSE', 'ON', 'OFF']:
                        raise FluidCmdCompileError(message="Argument {} of command {} is type LOGIC, but value {} was not in allowed set of ['HIGH', 'LOW', 'TRUE', 'FALSE', 'ON', 'OFF']".format(arg, cmd_info["name"], val))
                    else:
                        args_dict[arg]["value"] = val.upper()
                        if val.upper() in ['HIGH', 'TRUE', 'ON']:
                            compiled_args.extend(1 .to_bytes(1, 'big'))
                        else:
                            compiled_args.extend(0 .to_bytes(1, 'big'))
                case "STRING":
                    # Sigh. You know, I didn't want to handle strings. I knew I would have to at some point. Doesn't mean I wanted to.
                    if len(val) > args_dict[arg]["maxlen"]:
                        raise FluidCmdCompileError(message="Argument {} of command {} was longer than max string length {}!".format(arg, cmd_info["name"], args_dict[arg]["maxlen"]))
                    args_dict[arg]["value"] = val
                    compiled_args.extend(bytes(val, 'ascii'))
                case "ARRAY":
                    # Arrays are tough.
                    # This is a simplified implementation that ONLY allows arrays to have one-byte types for now.
                    # That said, the general concept holds.
                    array_len = args_dict[args_dict[arg]["len"]]["value"]
                    vals_in_array = input_args[args_dict[arg]["idx"]:args_dict[arg]["idx"]+array_len]
                    args_dict[arg]["value"] = []
                    for aval in vals_in_array:
                        if (int(aval) < args_dict[arg]["element_min"]) or (int(aval) > args_dict[arg]["element_max"]):
                            raise FluidCmdCompileError(message="Argument {} of command {} has value {} outside allowed range {}-{}!".format(arg, cmd_info["name"], aval, args_dict[arg]["element_min"], args_dict[arg]["element_max"]))
                        args_dict[arg]["value"].append(aval)
                        compiled_args.extend(int(aval).to_bytes(1, 'big'))
                    idx_array_offset += (array_len - 1)
                case _:
                    if args_dict[arg]["type"] not in self.special_types:
                        raise FluidCmdCompileError(message="Argument {} of command {} has type {} which is not yet supported by this cmd compiler!".format(arg, cmd_info["name"], args_dict[arg]["type"]))
                    else:
                        typedef = self.special_types[args_dict[arg]["type"]]
                        if val not in typedef:
                            raise FluidCmdCompileError(message="Argument {} of command {} is type {}, but value {} was not in type definition!".format(arg, cmd_info["name"], args_dict[arg]["type"], val))
                        else:
                            args_dict[arg]["value"] = val
                            compiled_args.extend(int(typedef[val]).to_bytes(1, 'big'))


        return compiled_args

class FluidLogDecodeError(Exception):
    def __init__(self, message="An exception occured during log message decoding!"):
        self.message = message
        super().__init__(self.message)

class FluidLogDecoder():
    def __init__(self):
        self.log_dict = {}
        self.log_levels = {}
        self.app_name = ""
        self.msgid_length = 0
        self.use_ccsds = False

    def is_initialized(self):
        if not self.log_dict:
            return False
        else:
            return True

    def load_log_dictionary(self, log_dict_path):
        with open(log_dict_path, "r") as f:
            full_log_dict = json.load(f)

        # Add later: file validation...
        self.app_name = full_log_dict["app_name"]
        self.msgid_length = full_log_dict["msgid_len"]
        self.use_ccsds = full_log_dict["use_ccsds"]
        self.log_levels = [None] * 1024 # This is wasteful, but accurately maps APID to log level.
        for level in full_log_dict["log_levels"]:
            self.log_levels[full_log_dict["log_levels"][level]] = level
        self.log_dict = {str(log["msgid"]): log for log in full_log_dict["messages"]}

    def list_logmsgs(self):
        return [self.log_dict[x]["name"] for x in self.log_dict]

    def get_app_name(self):
        return self.app_name

    def decode_log_message(self, data, cobs_decode=True):
        if cobs_decode:
            data = decode_from_recv(data)

        # Extract data from bytestream:
        log_level = ""
        msg_info = -1
        msg_data = None
        if self.use_ccsds:
            #TODO: Implement CCSDS
            pass
        else:
            if len(data) == 0:
                raise FluidLogDecodeError(message="Log message has zero length!")
            elif len(data) < (2 + self.msgid_length):
                raise FluidLogDecodeError(message="Log message minimum length is {}, but only {} bytes received!".format(2+self.msgid_length, len(data)))
            else:
                # Get log level from APID:
                apid = int.from_bytes(data[0:2], 'big')
                if self.log_levels[apid] == None:
                    raise FluidLogDecodeError(message="Message had APID {}, which does not correspond to any log level!".format(apid))
                else:
                    log_level = self.log_levels[apid]
                
                # Get message id:
                msg_id = int.from_bytes(data[2:2+self.msgid_length], 'big')
                if str(msg_id) not in self.log_dict:
                    raise FluidLogDecodeError(message="Message had log message ID {}, but this does not correspond to any message in the dictionary!".format(msg_id))
                else:
                    msg_info = self.log_dict[str(msg_id)]
                
                # Get arguments, if any:
                if len(data) > (2 + self.msgid_length):
                    msg_data = data[(2+self.msgid_length):]
                    if msg_info["args"] == []:
                        raise FluidLogDecodeError(message="Message {} is supposed to have no arguments, but arguments of length {} were found!".format(msg_info["name"], len(msg_data)))

        # Interpret data into a string:
        msg_str = ""
        if msg_info["args"] == []:
            msg_str = msg_info["fmt"]
        else:
            msg_str = self._decode_log_message_args(msg_data, msg_info)

        return log_level, msg_info["name"], msg_str

    def _decode_log_message_args(self, msg_arg_bytes, msg_info):
        # First, check that we have exactly the expected number of bytes in the message:
        expected_data_len = 0
        for arg in msg_info["args"]:
            match arg["type"]:
                case "U8" | "I8" | "LOGIC" | "ENUM":
                    expected_data_len += 1
                case "U16" | "I16":
                    expected_data_len += 2
                case "U32" | "I32":
                    expected_data_len += 4
                case _:
                    raise FluidLogDecodeError(message="Argument {} of message {} has unsupported type {}!".format(arg["name"], msg_info["name"], arg["type"]))
        if expected_data_len != len(msg_arg_bytes):
            raise FluidLogDecodeError(message="Message {} expected argument length {}, but received {} bytes!".format(msg_info["name"], expected_data_len, len(msg_arg_bytes)))

        # Now do the actual decoding:
        msg_arg_vals = []
        current_idx = 0
        for arg in msg_info["args"]:
            match arg["type"]:
                case "U8":
                    msg_arg_vals.append(int(msg_arg_bytes[current_idx]))
                    current_idx += 1
                case "U16":
                    msg_arg_vals.append(int.from_bytes(msg_arg_bytes[current_idx:current_idx+2], 'big'))
                    current_idx += 2
                case "U32":
                    msg_arg_vals.append(int.from_bytes(msg_arg_bytes[current_idx:current_idx+4], 'big'))
                    current_idx += 4
                case "I8":
                    msg_arg_vals.append(int.from_bytes(msg_arg_bytes[current_idx], 'big', signed=True))
                    current_idx += 1
                case "I16":
                    msg_arg_vals.append(int.from_bytes(msg_arg_bytes[current_idx:current_idx+2], 'big', signed=True))
                    current_idx += 2
                case "I32":
                    msg_arg_vals.append(int.from_bytes(msg_arg_bytes[current_idx:current_idx+4], 'big', signed=True))
                    current_idx += 4
                case "LOGIC":
                    raw_val = int(msg_arg_bytes[current_idx])
                    if raw_val not in [0, 1]:
                        raise FluidLogDecodeError(message="Argument {} of message {} is supposed to be a boolean LOGIC value, but had value {}!".format(arg["name"], msg_info["name"], raw_val))
                    msg_arg_vals.append(arg[str(raw_val)])
                    current_idx += 1
                case "ENUM":
                    raw_val = int(msg_arg_bytes[current_idx])
                    if not arg["range"][0] <= raw_val <= arg["range"][1]:
                        raise FluidLogDecodeError(message="Argument {} of message {} is supposed to be an ENUM with raw value in range {}-{}, but had value {}!".format(arg["name"], msg_info["name"], arg["range"][0], arg["range"][1], raw_val))
                    msg_arg_vals.append(arg[str(raw_val)])
                    current_idx += 1
                case _:
                    raise FluidLogDecodeError(message="Argument {} of message {} has unsupported type {}!".format(arg["name"], msg_info["name"], arg["type"]))
        
        msg_arg_tuple = tuple(msg_arg_vals)
        msg_str = msg_info["fmt"] % msg_arg_tuple
        return msg_str
